﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;

    public Animator anim;
    private TPS2 playerController;

    [HideInInspector] public Queue<string> sentences;
    [HideInInspector] public bool isOpen = false;
    public bool endDialogue = false;

    void Awake()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<TPS2>();
    }

    void Start()
    {
        sentences = new Queue<string>();
    }

    public void StartDialogue(Dialogue dialogue)
    {
        anim.SetBool("IsOpen", true);
        isOpen = true;
        nameText.text = dialogue.name;
        sentences.Clear();
        playerController.canMove = false;

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        dialogueText.text = sentence;
        Debug.Log(sentences.Count);
    }

    public void EndDialogue()
    {
        anim.SetBool("IsOpen", false);
        isOpen = false;
        playerController.canMove = true;
        endDialogue = true;
        Debug.Log("Dialogue Ended");
    }
}

// reference: https://www.youtube.com/watch?v=_nRzoTzeyxU