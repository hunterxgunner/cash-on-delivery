﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataHolderScript : MonoBehaviour
{
    public float earnedMoney = 0;

    public float totalMoney = 0;
    public int totalDeliveries = 0;
    public float finalMoney = 0;
    public float storedTotalRating = 0;
    public float finalRating = 0;
    public int currentDay = 0;
    public int maxNumberOfDays = 0;
}