﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public string newSceneName;     // default scene name for the next scene
    public string sceneLose;        // scene name for if the quota was not met
    public string sceneWin;         // scene name for if the quota was met
    public string sceneLowStars;    // scene name for if the stars count is too low

    private DataHolderScript dataHolder;

    private void Awake()
    {
        if (SceneManager.GetActiveScene().name != "Title")
        {
            dataHolder = GameObject.FindGameObjectWithTag("DataHolder").GetComponent<DataHolderScript>();
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveToNewScene()
    {
        SceneManager.LoadScene(newSceneName);
    }

    public void ResultsMoveToNewScene()
    {
        if (dataHolder.currentDay >= dataHolder.maxNumberOfDays)
        {
            CheckMoney();
        }
        else
        {
            SceneManager.LoadScene(newSceneName);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    void CheckRating()
    {
        if (dataHolder.finalRating < 3.0f)
        {
            SceneManager.LoadScene(sceneLowStars);
        }

    }

    void CheckMoney()
    {
        if (dataHolder.totalMoney > dataHolder.finalMoney && dataHolder.storedTotalRating/dataHolder.totalDeliveries > dataHolder.finalRating)
        {
            SceneManager.LoadScene(sceneWin);
        }
        else
        {
            SceneManager.LoadScene(sceneLose);
        }
    }
}