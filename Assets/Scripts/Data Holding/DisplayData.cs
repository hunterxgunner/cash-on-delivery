﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayData : MonoBehaviour
{
    [Header("Text UI Handlers")]
    public TextMeshProUGUI ratingText;
    public Image ratingStars;
    public TextMeshProUGUI earningsToday;
    public TextMeshProUGUI playerChat1, playerChat2;
    public TextMeshProUGUI managerChat1, managerChat2;

    private float money, totalMoney, rating, finalMoney, finalRating;
    private int currentDay, maxDays;

    private DataHolderScript dataHolder;

    private void Awake()
    {
        dataHolder = GameObject.FindGameObjectWithTag("DataHolder").GetComponent<DataHolderScript>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        money = dataHolder.earnedMoney;
        totalMoney = dataHolder.totalMoney;
        rating = dataHolder.storedTotalRating / dataHolder.totalDeliveries;
        finalMoney = dataHolder.finalMoney;
        finalRating = dataHolder.finalRating;
        currentDay = dataHolder.currentDay;
        maxDays = dataHolder.maxNumberOfDays;

        UpdateText();
    }

    void UpdateText()
    {
        ratingText.text = rating.ToString("f1");
        ratingStars.fillAmount = rating / 5;
        earningsToday.text = "$" + money.ToString("n2");
        playerChat1.text = "Sent over my <b>$" + money.ToString("n2") + "</b> earnings today to pay for rent.";
        managerChat1.text = "You still need <b>$" + (finalMoney - totalMoney).ToString("f2") + "</b> by the end of the week make sure you have it by then.";
        if (maxDays - currentDay != 1)
        {
            playerChat2.text = "That's in <b>" + (maxDays - currentDay).ToString() + " days</b> can't we get an extension?";
            managerChat2.text = "No. <b>$" + (finalMoney - totalMoney).ToString("f2") + " in " + (maxDays - currentDay).ToString() + " days</b> OR YOU WILL BE EVICTED.";
        }
        else
        {
            playerChat2.text = "That's <b>tomorrow</b> can't we get an extension?";
            managerChat2.text = "No. <b>$" + (finalMoney - totalMoney).ToString("f2") + " tomorrow</b> OR YOU WILL BE EVICTED.";
        }
    }
}