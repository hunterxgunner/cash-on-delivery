﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;
    public Transform playerLookAt;
    public Vector3 offset;

    public float rotateSpeed; // how fast the camera rotates
    public float minViewAngle;
    public float maxViewAngle;

    [Header("Camera 2.0")]
    public float lookSpeed = 2.0f;
    public float upperLookLimit = 60.0f;
    public float lowerLookLimit = 60.0f;
    Vector3 cameraRotation = Vector3.zero;
    public float collisionOffset = 0.2f; //To prevent Camera from clipping through Objects

    [Header ("Camera 3.0")]
    [SerializeField]
    private Vector2 cameraSensitivity = Vector2.one;

    [SerializeField]
    private float zoomSensitivity = 10.0f;

    [SerializeField]
    private Vector2 zoomClamp = new Vector2(0.5f, 25.0f);

    [SerializeField]
    private float zoomDamp = 0.05f;

    [SerializeField]
    private float cameraRadius = 0.5f;

    private float pitch = 0.0f, yaw = 0.0f;
    private float targetZoom = 5.0f, zoom = 5.0f;
    private float zoomVelocity = 0.0f;

    Vector3 defaultPos;
    Vector3 directionNormalized;
    Transform parentTransform;
    float defaultDistance;
    private bool activeCamera = true;

    public Transform pivot;

    void Start()
    {
        //offset = player.position - transform.position;

        pivot.transform.position = player.transform.position;
        //pivot.transform.parent = player.transform;
        pivot.transform.parent = null;
        cameraRotation.y = transform.eulerAngles.y;

        defaultPos = transform.position;
        directionNormalized = defaultPos.normalized;
        parentTransform = transform.parent;
        defaultDistance = Vector3.Distance(defaultPos, Vector3.zero);
    }
    
    void LateUpdate()
    {
        pivot.transform.position = player.transform.position;
        if (activeCamera)
        {
            NewCameraFunction();
        }
        transform.LookAt(playerLookAt);
    }

    void OldCameraFunction()
    {
        float horizontal = Input.GetAxisRaw("Mouse X") * rotateSpeed;
        pivot.Rotate(0.0f, horizontal, 0.0f);

        float vertical = Input.GetAxisRaw("Mouse Y") * rotateSpeed;
        //vertical = Mathf.Clamp(vertical, -2, 2); will sort this out later
        pivot.Rotate(-vertical, 0.0f, 0.0f);

        //pivot.rotation = Quaternion.Euler(Mathf.Clamp(pivot.eulerAngles.x, 0.0f, maxViewAngle), pivot.eulerAngles.y, 0.0f);

        if (pivot.rotation.eulerAngles.x > maxViewAngle && pivot.rotation.eulerAngles.x < 180.0f)
        {
            pivot.rotation = Quaternion.Euler(maxViewAngle, pivot.eulerAngles.y, 0.0f);
        }
        if (pivot.rotation.eulerAngles.x > 180.0f && pivot.rotation.eulerAngles.x < 360.0f + minViewAngle)
        {
            pivot.rotation = Quaternion.Euler(360.0f + minViewAngle, pivot.eulerAngles.y, 0.0f);
        }

        float desiredYAngle = pivot.eulerAngles.y;
        float desiredXAngle = pivot.eulerAngles.x;
        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0.0f);
        transform.position = player.position - (rotation * offset);

        //transform.position = player.position - offset;

        if (transform.position.y < player.position.y)
        {
            transform.position = new Vector3(transform.position.x, player.position.y, transform.position.z);
        }
    }

    void NewCameraFunction()
    {
        cameraRotation.y += Input.GetAxisRaw("Mouse X") * lookSpeed;
        cameraRotation.x += -Input.GetAxisRaw("Mouse Y") * lookSpeed;
        cameraRotation.x = Mathf.Clamp(cameraRotation.x, -lowerLookLimit, upperLookLimit);
        pivot.localRotation = Quaternion.Euler(cameraRotation.x, cameraRotation.y, 0);

        float desiredYAngle = pivot.eulerAngles.y;
        float desiredXAngle = pivot.eulerAngles.x;
        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0.0f);
        transform.position = player.position - (rotation * offset);
        //CollisionDetection();

        /*if (transform.position.y < player.position.y)
        {
            transform.position = new Vector3(transform.position.x, player.position.y, transform.position.z);
        }*/
        //transform.eulerAngles = new Vector2(0, rotation.y);

        //float horizontal = Input.GetAxisRaw("Mouse X") * lookSpeed;
        //pivot.Rotate(0.0f, horizontal, 0.0f);
        //pivot.rotation = Quaternion.Euler(0.0f, pivot.eulerAngles.y, 0.0f);
    }

    // legit I'm getting tired of this
    void YetAnotherCameraFunction() // REQUIRES THE CAMERA TO BE A CHILD OF THE PLAYER
    {
        CameraRotate();
        CameraCollision();
    }

    void CameraRotate()
    {
        pitch -= Input.GetAxisRaw("Mouse Y") * lookSpeed;
        yaw += Input.GetAxisRaw("Mouse X") * lookSpeed;

        pitch = Mathf.Clamp(pitch, -lowerLookLimit, upperLookLimit);
        pivot.localRotation = Quaternion.Euler(pitch, yaw, 0.0f);
    }

    void CameraCollision()
    {
        Ray ray = new Ray(playerLookAt.position, -playerLookAt.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, targetZoom))
        {
            zoom = Mathf.SmoothDamp(zoom, hit.distance, ref zoomVelocity, zoomDamp);
            //zoom -= cameraRadius;
        }
        else
        {
            zoom = Mathf.SmoothDamp(zoom, targetZoom, ref zoomVelocity, zoomDamp);
        }
        zoom = Mathf.Clamp(zoom, zoomClamp.x, zoomClamp.y);
        transform.localPosition = Vector3.back * zoom;
    }

    void CollisionDetection()
    {
        Vector3 currentPos = defaultPos;
        RaycastHit hit;
        Vector3 dirTmp = playerLookAt.position - player.transform.position;
        if (Physics.SphereCast(playerLookAt.position, collisionOffset, dirTmp, out hit, defaultDistance))
        {
            currentPos = (directionNormalized * (hit.distance - collisionOffset));
        }

        transform.position = Vector3.Lerp(transform.position, currentPos, Time.deltaTime * 15f);
    }

    public void ActivateCamera()
    {
        activeCamera = true;
    }

    public void DeactivateCamera()
    {
        activeCamera = false;
    }
}

// https://forum.unity.com/threads/complete-camera-collision-detection-third-person-games.347233/