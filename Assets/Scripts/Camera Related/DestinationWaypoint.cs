﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DestinationWaypoint : MonoBehaviour
{
    public Image waypointMarker;            // the waypoint marker in the canvas; manually set
    public Transform target;                // the target game object
    public TextMeshProUGUI distanceText;    // the distance text field in the canvas, manually set
    public Vector3 offset;                  // offset?
    public RawImage miniMapCanvas;          // the canvas on the cellphone during minimap view
    public GameObject mapMarker;            // the on world map marker for the cellphone view; automatically set

    [Header("Scale Options")]
    public float distance;
    public float minScale;
    public float maxScale;
    public Vector3 scalar;

    private GameObject player;
    private GameObject worldMarker;

    private float minX, maxX;
    private float minY, maxY;
    private float dist;

    private void Awake()
    {
        player = GameObject.FindWithTag("Player");
    }

    public void Start()
    {
        waypointMarker.gameObject.SetActive(false);
        worldMarker = Instantiate(mapMarker);
        worldMarker.SetActive(false);
    }

    void LateUpdate()
    {
        StayInScreen();
        if (target)
        {
            worldMarker.SetActive(true);
            dist = Vector3.Distance(target.position, player.transform.position);
            if (dist < 16.0f)
            {
                EnableMarker();
                TrackTarget();
                ChangeSize();
            }
            else
            {
                DisableMarker();
            }
        }
        else
        {
            worldMarker.SetActive(false);
            DisableMarker();
        }
    }

    void StayInScreen()
    {
        minX = waypointMarker.GetPixelAdjustedRect().width / 2;
        maxX = Screen.width - minX;

        minY = waypointMarker.GetPixelAdjustedRect().height / 2;
        maxY = Screen.height - minY;
    }

    void TrackTarget()
    {
        Vector2 pos = Camera.main.WorldToScreenPoint(target.position + offset);

        if (Vector3.Dot((target.position - transform.position), transform.forward) < 0)
        {
            // target is behind the player
            if (pos.x < Screen.width / 2)
            {
                pos.x = maxX;
            }
            else
            {
                pos.x = minX;
            }
        }

        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);

        waypointMarker.transform.position = pos;
        distanceText.text = ((int)Vector3.Distance(target.position, player.transform.position)).ToString() + "m";
    }

    void MinimapTrackTarget()
    {
        //Vector2 pos = miniMapCanvas.m.WorldToScreenPoint(target.position + offset);
    }

    void ChangeSize()
    {
        Vector3 newScale = scalar / Mathf.Clamp((dist / distance), minScale, maxScale);

        //Debug.Log(dist / distance);
        waypointMarker.gameObject.transform.localScale = newScale;
    }

    public void EnableMarker()
    {
        waypointMarker.gameObject.SetActive(true);
    }

    public void DisableMarker()
    {
        waypointMarker.gameObject.SetActive(false);
    }
}

// reference : https://www.youtube.com/watch?v=oBkfujKPZw8