﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;
using TMPro;

public class CellphoneState : MonoBehaviour
{
    [Header("Standard Cellphone Assets")]
    public GameObject cellphone;                // the cellphone game object, the entirety of it
    private Animator cellAnimator;              // the cellphone animator to make it animate
    public GameObject mainMap;                  // the main map game object, the big one
    public GameObject miniMap;                  // the minimap game object, the one you see at your side
    public GameObject app;                      // the application game object, the one where you bring up your cellphone


    public bool enableSwapping;                 // bool that allows the buttons to be pressed
    private CameraController cameraController;  // the camera controller component, frequently used to enable and disable movement

    void Start()
    {
        enableSwapping = true;
        cellAnimator = cellphone.GetComponent<Animator>();
        Cursor.lockState = CursorLockMode.Locked;
        cameraController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enableSwapping)
        {
            if (Input.GetButtonDown("Map"))
            {
                if (!mainMap.activeSelf)
                {
                    SelectMap();
                }
                else
                {
                    SelectDefault();
                }
            }

            if (Input.GetButtonDown("OpenApp"))
            {
                if (!app.activeSelf)
                {
                    SelectApp();
                }
                else
                {
                    SelectDefault();
                }
            }
        }
    }
    public void SelectDefault()
    {
        mainMap.SetActive(false);
        miniMap.SetActive(true);
        app.SetActive(false);
        cellAnimator.SetTrigger("ToDefault");

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        cameraController.ActivateCamera();
    }

    public void SelectMap()
    {
        mainMap.SetActive(true);
        miniMap.SetActive(false);
        app.SetActive(false);
        cellAnimator.SetTrigger("ToWorldMap");

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        cameraController.ActivateCamera();
    }

    public void SelectApp()
    {
        mainMap.SetActive(false);
        miniMap.SetActive(false);
        app.SetActive(true);
        cellAnimator.SetTrigger("ToApp");

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        cameraController.DeactivateCamera();
        GameObject.FindGameObjectWithTag("Cellphone").GetComponent<ListMaster>().RefreshCustomers();
    }
    
    public void ConfirmJob()
    {
        mainMap.SetActive(false);
        miniMap.SetActive(false);
        app.SetActive(false);
        cellAnimator.SetTrigger("ToApp");

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        cameraController.DeactivateCamera();
    }
}