﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CellphoneDisplayData : MonoBehaviour
{
    [Header("Main Application Page")]
    public TextMeshProUGUI moneyMA;
    public TextMeshProUGUI ratingMA;

    [Header("Profile Page")]
    public TextMeshProUGUI moneyPP;
    public TextMeshProUGUI ratingPP;

    private QuestMaster questMaster;
    private DataHolderScript dataHolder;

    void Awake()
    {
        questMaster = GameObject.FindGameObjectWithTag("GameController").GetComponent<QuestMaster>();
        dataHolder = GameObject.FindGameObjectWithTag("DataHolder").GetComponent<DataHolderScript>();
    }

    void Start()
    {
        moneyMA.text = "$" + dataHolder.totalMoney;
        ratingMA.text = questMaster.rating.ToString("f1");

        moneyPP.text = "$" + dataHolder.totalMoney;
        ratingPP.text = questMaster.rating.ToString("f1");
    }

    void Update()
    {
        moneyMA.text = "$" + dataHolder.totalMoney;
        ratingMA.text = questMaster.rating.ToString("f1");

        moneyPP.text = "$" + dataHolder.totalMoney;
        ratingPP.text = questMaster.rating.ToString("f1");
    }
}