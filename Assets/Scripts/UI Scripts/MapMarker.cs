﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMarker : MonoBehaviour
{
    public GameObject target;
    public DestinationWaypoint waypointScript;
    public GameObject pulse;
    public float pulseSpeed;
    private Vector3 pulseScaler = new Vector3(100.0f, 100.0f, 100.0f);

    void Awake()
    {
        waypointScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<DestinationWaypoint>();
    }

    void Start()
    {
        pulseScaler = new Vector3(pulseSpeed, pulseSpeed, pulseSpeed);
    }

    void Update()
    {
        if (waypointScript.target)
        {
            transform.position = waypointScript.target.position;
            PulseGraphic();
        }
    }

    void PulseGraphic()
    {
        pulse.transform.localScale += pulseScaler * Time.deltaTime;
        if (pulse.transform.localScale.x > 200.0f)
        {
            pulse.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }
    }
}