﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CustomerButtonScript : MonoBehaviour
{
    [Header("Main Holding Variables")]
    public Button acceptButton;
    public DestinationWaypoint waypoint;
    public GameObject targetObject;         // target game object, used for interaction
    public string targetName;               // target name
    public string itemName;                 // target's item name
    public float timeLimit;                 // delivery time limit
    public int reward;                      // delivery reward

    [Header("Text Variables")]
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI itemText;
    public TextMeshProUGUI timeLimitText;
    public TextMeshProUGUI rewardText;

    void Awake()
    {
        waypoint = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<DestinationWaypoint>();
    }

    public void RefreshVariables()
    {
        nameText.text = name;
        itemText.text = itemName;
        timeLimitText.text = timeLimit.ToString();
        rewardText.text = reward.ToString();

        if (!GameObject.FindGameObjectWithTag("GameController").GetComponent<QuestMaster>().activeQuest)
        {
            acceptButton.interactable = true;
        }
        else
        {
            acceptButton.interactable = false;
        }
    }

    public void SelectJob()
    {
        waypoint.target = targetObject.transform;
        //waypoint.EnableMarker();
        GameObject.FindGameObjectWithTag("Cellphone").GetComponent<CellphoneState>().SelectDefault();
    }
}