﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestHolder : MonoBehaviour
{
    private DialogueManager dialogueManager;
    private DialogueTrigger dialogueTrigger;
    private QuestMaster questMaster;
    //private CellphoneState cellphoneState;
    private DestinationWaypoint waypointScript;
    QuestHolder partnerQuestHolder;     // the partner's questholder variable

    [Header("Quest Options")]
    public bool questGiver;             // IMPORTANT: Mark if it is the quest giver
    public int questID;                 // match this with quest start to end
    public GameObject questPartner;     // the partner quest object, from both sides
    public string questItem;            // the item's name
    public bool questFinished;          // boolean that determines if the quest is finished
    public bool finishedTalking;        // boolean that determines if the object is finished speaking

    [Header("Quest Finish")]
    public int baseMoney;               // reward; money
    public float timeLimit;             // time limit of delivery in seconds
    //public float silverTime;          // silver time limit // anything else is bronze

    void Awake()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
        dialogueTrigger = gameObject.GetComponent<DialogueTrigger>();
        questMaster = GameObject.FindWithTag("GameController").GetComponent<QuestMaster>();
        //cellphoneState = GameObject.FindWithTag("GameController").GetComponent<CellphoneState>();

        waypointScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<DestinationWaypoint>();
        partnerQuestHolder = questPartner.GetComponent<QuestHolder>();

        int distancePerPay = (int)Vector3.Distance(transform.position, questPartner.transform.position);
        baseMoney = distancePerPay;
    }

    void Start()
    {
        if (!questGiver)
        {
            questID = partnerQuestHolder.questID;
            questItem = partnerQuestHolder.questItem;
            baseMoney = partnerQuestHolder.baseMoney;
            timeLimit = partnerQuestHolder.timeLimit;
            //silverTime = partnerQuestHolder.silverTime;
        }
    }

    void Update()
    {
        
    }

    public void OnTriggerStay(Collider other)
    {
        /*
        // if object can give a quest and no quest is active
        if (questGiver && !questFinished && !questMaster.activeQuest)
        {
            ConversationStart(other);
        }
        // if object receives quest item and a quest is active
        else if (!questGiver && questMaster.activeQuest)
        {
            QuestEnder(other);
        }*/

        ConversationBranch(other);
    }

    public void OnTriggerExit(Collider other)
    {
        if (dialogueManager.isOpen)
        {
            dialogueManager.EndDialogue();
        }
    }

    public void ConversationBranch(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetButtonDown("Interact") && !finishedTalking) // if not finished talking
        {
            if (questGiver && !questMaster.activeQuest)
            {
                Conversation();
            }
            else if (!questGiver && questMaster.currentQuestID == questID)
            {
                Conversation();
            }
        }
    }

    public void Conversation()
    {
        if (dialogueManager.isOpen == false)
        {
            dialogueTrigger.TriggerDialogue();
            if (!questGiver && questMaster.currentQuestID == questID)
            {
                QuestEnder();
            }
        }
        else
        {
            dialogueManager.DisplayNextSentence();
            if (dialogueManager.endDialogue)
            {
                if (questGiver)
                {
                    QuestStart();
                }
                dialogueManager.endDialogue = false;
                finishedTalking = true;
            }
        }
    }

    public void QuestStart()
    {
        //waypointScript.EnableMarker();
        waypointScript.target = questPartner.transform;
        questMaster.currentQuestID = questID;
        questMaster.currentWatch = 0;
        questMaster.activeQuest = true;
        questMaster.maxTimer = timeLimit;
        questMaster.ActivateTimers();
    }

    public void QuestEnder()
    {
        //waypointScript.DisableMarker();
        waypointScript.target = null;
        questMaster.activeQuest = false;
        questMaster.Rewarding(baseMoney);

        questFinished = true;
        partnerQuestHolder.questFinished = true;

        if (questMaster.rating < 3)
        {
            // game over screen
        }
    }
}

// scroll cellphone: https://www.youtube.com/watch?v=MWOvwegLDl0
// https://www.youtube.com/watch?v=lUun2xW6FJ4