﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class WorldTimer : MonoBehaviour
{
    public GameObject sun;          // the sun game object
    public float dayTimeLimit;      // the time limit in seconds on how long a day is
    public int currentDay = 1;      // the current day
    public int maxNumberOfDays;     // how many days before the game ends
    public TextMeshProUGUI dayText; // the text holder for the day
    public Image sunImage;
    float countUp = 0;
    float countDown = 0;
    Animation sunAnimation;

    private void Awake()
    {
        GameObject sunObject = Instantiate(sun);
        sunAnimation = sunObject.GetComponent<Animation>();
        sunAnimation.Play("SunRotation");
        sunAnimation["SunRotation"].speed = 0;
        countDown = dayTimeLimit;
    }

    private void Start()
    {
        currentDay += GameObject.FindGameObjectWithTag("DataHolder").GetComponent<DataHolderScript>().currentDay;
    }

    void Update()
    {
        TickUp();
        DisplayTimer();
    }

    void TickUp()
    {
        countUp += Time.deltaTime;
        float counter = countUp / dayTimeLimit;
        counter = Mathf.Clamp(counter, 0.0f, 1.0f);

        sunAnimation["SunRotation"].normalizedTime = counter;

        if (counter >= 1)
        {
            EndDay();
        }
    }

    void DisplayTimer()
    {
        countDown -= Time.deltaTime;
        int seconds = (int)(countDown % 60);
        int minutes = (int)(countDown / 60) % 60;
        //int hours = (int)(countDown / 3600) % 24;

        string stopwatchString = string.Format("{0:0}:{1:00}", minutes, seconds);
        dayText.text = stopwatchString;
        sunImage.fillAmount = countDown/dayTimeLimit;
    }

    void EndDay()
    {
        GetComponent<QuestMaster>().PushAllData();
        SceneManager.LoadScene("DailyResults");
    }
}

// https://answers.unity.com/questions/1225328/imported-animated-object-and-slider-tutorial.html