﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListMaster : MonoBehaviour
{
    public GameObject[] customers;
    public GameObject customerButton;
    public Transform cellphoneParent;

    // Start is called before the first frame update
    void Start()
    {
        customers = GameObject.FindGameObjectsWithTag("StartingLine");

        foreach (GameObject customer in customers)
        {
            RefreshCustomers();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RefreshCustomers()
    {
        if (cellphoneParent.childCount > 0)
        {
            foreach (Transform child in cellphoneParent)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        foreach (GameObject customer in customers)
        {
            if (!customer.GetComponent<QuestHolder>().questFinished)
            {
                GameObject CustomerButton =  Instantiate(customerButton, cellphoneParent);

                CustomerButtonScript buttonScript = CustomerButton.GetComponent<CustomerButtonScript>();


                buttonScript.targetObject = customer.gameObject;
                buttonScript.name = customer.GetComponent<DialogueTrigger>().dialogue.name;
                buttonScript.itemName = customer.GetComponent<QuestHolder>().questItem;
                buttonScript.timeLimit = customer.GetComponent<QuestHolder>().timeLimit;
                buttonScript.reward = customer.GetComponent<QuestHolder>().baseMoney;

                buttonScript.RefreshVariables();
            }
        }
    }
}

// source : https://docs.unity3d.com/ScriptReference/GameObject.FindGameObjectsWithTag.html