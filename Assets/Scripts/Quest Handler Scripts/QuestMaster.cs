﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestMaster : MonoBehaviour
{
    [Header("Quest Variables")]
    public bool activeQuest;                        // check if a quest is active
    public int currentQuestID;                      // current ID of the quest
    public float currentWatch;                      // stopwatch; starts a 0

    [Header("Quest Endgoals")]
    public float finalMoney;                        // required money for the good ending
    public float finalRating;                       // required rating for the good ending

    [Header("Player Statistics")]
    public float rating = 5.0f;                     // rating of the player; starts at 5 stars
    public int money = 0;                           // money earned for today
    private float ratingTotal = 5.0f;               // total rating; points added based on each rating giving
    public int deliveriesFinished = 0;              // total deliveries finished
    private DataHolderScript dataHolder;            // Data holder for transfering data between scenes; automatic

    [Header("Quest UI Handlers")]
    public TextMeshProUGUI stopwatchUI;             // stopwatch UI; might be removed soon
    public GameObject star;                         // star gameobject. Used for counting the amount of stars for rating
    public Transform starHolder;                    // transform of where the stars should be placed

    // variables for star deletion
    public float maxTimer;                          // timer before the first star is removed
    float decayMaxTimer;                            // timer cap for fading stars
    GameObject[] starArray = new GameObject[5];     // array of stars for counting
    [SerializeField]float decayTimer = 0;           // stopwatch timer for star decay; in seconds

    void Awake()
    {
        dataHolder = GameObject.FindGameObjectWithTag("DataHolder").GetComponent<DataHolderScript>();
    }
    void Start()
    {
        deliveriesFinished = dataHolder.totalDeliveries;
        ratingTotal = dataHolder.storedTotalRating;
        if (deliveriesFinished == 0)
        {
            rating = ratingTotal;
        }
        else
        {
            rating = ratingTotal / deliveriesFinished;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (activeQuest)
        {
            DoQuest();
        }
    }

    public void DoQuest()
    {
        Stopwatch();
    }

    public void Stopwatch()
    {
        currentWatch += Time.deltaTime;
        int seconds = (int)(currentWatch % 60);
        int minutes = (int)(currentWatch / 60) % 60;
        //int hours = (int)(currentWatch / 3600) % 24;

        //string stopwatchString = string.Format("{0:0}:{1:00}:{2:00}", hours, minutes, seconds);
        string stopwatchString = string.Format("{0:00}:{1:00}", minutes, seconds);
        stopwatchUI.text = stopwatchString;

        DecayStars();
    }

    public void ActivateTimers()
    {
        stopwatchUI.gameObject.SetActive(true);
        StarTimers();
    }

    public void StarTimers()
    {
        decayTimer = maxTimer;
        decayMaxTimer = maxTimer;
        for (int i = 0; i < 5; i++)
        {
            GameObject starObject = Instantiate(star, starHolder);
            starArray[i] = starObject;
        }
    }

    public void DecayStars()
    {
        int starCount = starHolder.childCount;
        starArray[starCount - 1].GetComponent<Image>().fillAmount = decayTimer / decayMaxTimer;

        decayTimer -= Time.deltaTime;
        if (decayTimer <= 0 && starCount > 1)
        {
            Destroy(starArray[starCount - 1]);
            decayTimer = 10;
            decayMaxTimer = 10;
        }
    }

    public void DestroyStars()
    {
        foreach (Transform child in starHolder)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void Rewarding(int baseMoney)
    {
        // if currentTime < goldTime; get bonus and get 5*
        // if currentTime > goldTime but currentTime < silverTime; get small bonus and get 4*
        // if currentTime > silverTime; get OOFed

        if (starHolder.childCount >= 5)
        {
            money += (int)(baseMoney + (baseMoney * 0.5));
        }
        else if (starHolder.childCount == 4)
        {
            money += (int)(baseMoney + (baseMoney * 0.3));
        }
        else if (starHolder.childCount == 3)
        {
            money += (int)(baseMoney + (baseMoney * 0.1));
        }
        else
        {
            money += baseMoney;
        }
        ratingTotal += starHolder.childCount;
        deliveriesFinished++;
        DestroyStars();
        AverageCalculation();
        dataHolder.totalMoney += money;
        stopwatchUI.gameObject.SetActive(false);
    }

    public void AverageCalculation()
    {
        rating = (ratingTotal / deliveriesFinished);
    }

    public void PushAllData()
    {
        dataHolder.earnedMoney = money;
        dataHolder.finalMoney = finalMoney;
        dataHolder.totalDeliveries += deliveriesFinished;
        dataHolder.storedTotalRating += ratingTotal;
        dataHolder.finalRating = finalRating;
        dataHolder.currentDay = GetComponent<WorldTimer>().currentDay;
        dataHolder.maxNumberOfDays = GetComponent<WorldTimer>().maxNumberOfDays;
    }
}

// stopwatch source: https://www.youtube.com/watch?v=ixlIaMuhkbM